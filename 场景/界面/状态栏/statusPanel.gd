extends HBoxContainer

@export var stats:Stats
@onready var healthBar:TextureProgressBar=$VBox/HealthBar
@onready var redBar:TextureProgressBar=$VBox/HealthBar/RedBar
@onready var energyBar:TextureProgressBar=$VBox/EnergyBar

func _ready() -> void:
	if not stats:stats=Game.playerStats
	stats.healthChanged.connect(updateHealth)
	updateHealth(true)

	stats.energyChanged.connect(updateEnergy)
	updateEnergy()

	# 当前挂载的场景树退出时，断开连接
	tree_exited.connect(func():
		stats.healthChanged.disconnect(updateHealth)
		stats.energyChanged.disconnect(updateEnergy)
	)

# 初始加载人物不做血量动画
func updateHealth(skip:=false)->void:
	var percent:=stats.health/float(stats.max_health)
	healthBar.value=percent
	if skip:
		redBar.value=percent
	else:
		create_tween().tween_property(redBar,'value',percent,0.3)

func updateEnergy()->void:
	var percent:=stats.energy/(stats.max_energy)
	energyBar.value=percent
	create_tween().tween_property(energyBar,'value',percent,0.3)
