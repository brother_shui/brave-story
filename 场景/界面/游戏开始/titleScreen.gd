extends Control

@export var bgm:AudioStream
@onready var vBox:VBoxContainer=$VBoxContainer
@onready var newGame:Button=$VBoxContainer/NewGame
@onready var loadGame:Button=$VBoxContainer/LoadGame

func _ready() -> void:
    loadGame.disabled=!Game.hasSave()
    # 自动聚焦
    newGame.grab_focus()
        
    # 为按钮绑定音效
    Sounds.bindEvent(self)
    playBgm()

func playBgm()->void:
    if bgm:Sounds.playBgm(bgm)

func _on_new_game_pressed() -> void:
    Game.newGame()

func _on_load_game_pressed() -> void:
    Game.loadGame()

func _on_exit_game_pressed() -> void:
    get_tree().quit()




