extends Control

@onready var resume:Button=$V/Actions/H/Resume
@onready var slider1:HSlider=$V/AudioSetting/G/VolumeSlide
@onready var slider2:HSlider=$V/AudioSetting/G/VolumeSlide2
@onready var slider3:HSlider=$V/AudioSetting/G/VolumeSlide3

func _ready() -> void:
	hide()
	Sounds.bindEvent(self)
	visibility_changed.connect(func ():
		get_tree().paused=visible
	)
func _input(event: InputEvent) -> void:
	if event.is_action_pressed("pause") || event.is_action_pressed('ui_cancel'):
		hide()
		get_window().set_input_as_handled()

func update()-> void:
	slider1.update()
	slider2.update()
	slider3.update()

func showPause()->void:
	show()
	resume.grab_focus()

func _on_resume_pressed() -> void:
	hide()

func _on_quit_pressed() -> void:
	if (Game.curSceneName()=='titleScreen'):return get_tree().quit()
	hide()
	Game.backTitle()



	
