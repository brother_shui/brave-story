extends Control

@export var bgm:AudioStream
@onready var label=$Label

const LINES:=[
	"在无尽的征途中\n他们终于找到了失落的文明\n拯救了濒临毁灭的世界",
	"新的黎明在地平线上升起\n带来了和平与繁荣",
	"他们的勇气和智慧铸就了永恒的传奇\n成为历史长河中闪耀的星辰",
	"而他们的故事\n将永远被人们传颂\n直至万古长存"
]

var current:=1
var tween:Tween

func _ready() -> void:
	playBgm()
	showLine(0)

func _input(event: InputEvent) -> void:
	if tween.is_running():return

	if (
		event is InputEvent or 
		event is InputEventMouseButton or 
		event is InputEventJoypadButton
	):
		# 按钮按下，且不是回显事件（按住不放时反复触发的事件）
		if event.is_pressed() and not event.is_echo():
			if current+1<LINES.size():
				showLine(current+1)
			else:
				Game.backTitle()

# 显示文本
func showLine(line:int)->void:
	current=line

	tween=create_tween()
	tween.set_ease(Tween.EASE_IN_OUT).set_trans(Tween.TRANS_SINE)

	#  前一行渐隐
	if !line:
		label.modulate.a=0
	else:
		tween.tween_property(label,"modulate:a",0,1)
	# 动画结束后，label文本更新
	tween.tween_callback(label.set_text.bind(LINES[line]))
	#  后一行渐现
	tween.tween_property(label,"modulate:a",1,1)

func playBgm()->void:
	if bgm:Sounds.playBgm(bgm)

