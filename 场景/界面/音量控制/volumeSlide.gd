extends HSlider

@export var bus:StringName='Master'

@onready var busIndex:=AudioServer.get_bus_index(bus)

func _ready()->void:
    value_changed.connect(setValue)

func setValue(v:float):
    Sounds.setVolume(busIndex,v)
    Game.saveConfg()

func update()->void:
    value=Sounds.getVolume(busIndex)
    # print(busIndex,bus,value)