extends Control
@onready var animation:AnimationPlayer=$AnimationPlayer

func _ready() -> void:
    visibility_changed.connect(func ():
        set_process_input(visible)
        if visible:
            animation.play('enter')
        else:
            animation.play('RESET')
        )
    hide()

func _input(event: InputEvent) -> void:
    if animation.is_playing():return
    # 屏蔽事件
    get_window().set_input_as_handled()

    if (
        event is InputEvent or 
        event is InputEventMouseButton or 
        event is InputEventJoypadButton
    ):
        # 按钮按下，且不是回显事件（按住不放时反复触发的事件）
        if event.is_pressed() and not event.is_echo():
            hide()
            if Game.hasSave():
                Game.loadGame()
            else:
                Game.backTitle()