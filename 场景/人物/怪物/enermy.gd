class_name Enemy
extends CharacterBody2D

enum Direction{
	LEFT=-1,
	RIGHT=1
}
signal died

@export var direction:int=Direction.LEFT:
	set(v):
		direction=v
		if not is_node_ready():
			await ready
		graphics.scale.x=-direction

@export var maxSpeed:float=180
@export var acceleration:float=2000

var gravity := ProjectSettings.get("physics/2d/default_gravity") as float

@onready var graphics:Node2D=$Graphics
@onready var animationPlayer:AnimationPlayer=$AnimationPlayer
@onready var stateMachine:Node=$StateMachine
@onready var stats:Node=$Stats


func _ready() -> void:
	add_to_group('enemies')

# 移动ai
func move(speed:float,delta:float)->void:
	velocity.x=move_toward(velocity.x, speed*direction,acceleration*delta)
	velocity.y+=gravity*delta
	move_and_slide()

# 死亡
func die()->void:
	died.emit()

