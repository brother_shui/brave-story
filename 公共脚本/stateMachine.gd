class_name StateMachine
extends Node

const KEEP_CURRENT := -1
var stateTime: float # 当前状态的持续时间
var currentState := -1:
    set(v):
        owner.transitionState(currentState, v)
        currentState = v
        stateTime = 0

# 此函数的调用顺序为子节点ready后父节点ready
func _ready() -> void:
    await owner.ready
    currentState = 0

func _physics_process(delta: float) -> void:
    var next := owner.getNextState(currentState) as int
    if next != KEEP_CURRENT:
        currentState = next
    owner.tickPhysics(currentState, delta)
    stateTime += delta