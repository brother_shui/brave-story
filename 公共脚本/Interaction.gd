extends AnimatedSprite2D

# 触屏设备 DisplayServer.is_touchscreen_available()

const STICK_DEADZONE:=0.3
const MOUSE_DEADZONE:=16.0
# ---------------------生命周期-----------------------#
func _ready()->void:
    if Input.get_connected_joypads():
        showPadIcon(0)
    elif DisplayServer.is_touchscreen_available():
        print('触屏设备')
    else:
        play('keyboard')

# 根据输入变更按键提示，设置死区
func _input(event: InputEvent) -> void:
    if (
        event is InputEventJoypadButton or
        (event is InputEventJoypadMotion and abs(event.axis_value)>STICK_DEADZONE)
    ):
        showPadIcon(event.device)
    if (
        event is InputEventKey or 
        event is InputEventMouseButton or 
        (event is InputEventMouseMotion and event.velocity.length()>MOUSE_DEADZONE)
    ):
        play('keyboard')

func showPadIcon(device:int)->void:
    var joypadName:=Input.get_joy_name(device)
    if "Nintendo" in joypadName:
        play('nintendo')
    elif "DualShock" in joypadName or "PS" in joypadName:
         play('playStation') # playStation
    else:
        play('xbox') # xbox