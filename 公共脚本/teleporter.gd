class_name Teleporter
extends Interact

@export_file('*.tscn') var path:String
@export var entryPoint:String

func interact()->void:
	super()
	Game.changeScene(path,{entryPoint=entryPoint})
