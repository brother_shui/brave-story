extends Camera2D

# 恢复速度
@export var recoverySpeed:=16.0
var strength := 0.0


func _ready()->void:
    Game.cameraShake.connect(func (amount:float):
        strength+=amount
    )

func _process(delta: float) -> void:
    offset =Vector2(
        randf_range(-strength,+strength),
        randf_range(-strength,+strength)
    )
    strength = move_toward( strength,0,recoverySpeed*delta )