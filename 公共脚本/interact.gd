class_name Interact
extends Area2D

signal interacted

func _init() -> void:
    collision_layer=0
    collision_mask=0
    set_collision_mask_value(2,true)
    # 连接信号到函数
    body_entered.connect(_on_body_entered)
    body_exited.connect(_on_body_exited)
    
# 交互事件
func interact()->void:
    print('[interact] %s' % name)
    interacted.emit()

func _on_body_entered(player:Player)->void:
    player.addInteract(self)

func _on_body_exited(player:Player)->void:
    player.removeInteract(self)
