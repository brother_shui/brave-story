class_name Stats
extends Node
# ---------------------变量声明-----------------------#
signal healthChanged
signal energyChanged

# 导出变量的初始化在普通变量之后
@export var max_health:int=3
@export var max_energy:float=10
# 精力每秒恢复点数
@export var energy_regen:float=0.8

@onready var health:int=max_health:
    set(v):
        v=clampi(v,0,max_health)
        if health==v:return
        health=v
        healthChanged.emit()
@onready var energy:float=max_energy:
    set(v):
        v=clampf(v,0,max_energy)
        if energy==v:
            return
        energy=v
        energyChanged.emit()

# ---------------------生命周期-----------------------#
func _process(delta: float) -> void:
    energy+=energy_regen*delta

# ---------------------响应函数-----------------------#
# 统计数据取出
func toDict()->Dictionary:
    return {
		max_health=max_health,
        max_energy=max_energy,
        health=health
	}

# 统计数据赋值
func fromDict(dict:Dictionary)->void:
    max_energy=dict.max_energy
    max_health=dict.max_health
    health=dict.health
